﻿<#
MOSCOW INDUSTRIAL BANK.
Data 29.07.2015
Script for added DNS record to DNS server
By Sharikov Pavel
#>

# Обьявление переменных не требуется, можно сразу писать значения в переменную. Система автоматические переменную превращает в массив если в неё попадает целый набор значений.

# Считывание строк из файла можно сделать с помощью простой инструкции.
# $data = Get-Content <имя файла в кавычках или переменная>
# $data[0] - считывание первой строки
# $data[1] - считывание второй строки и т.д.

# Функция отображения ошибки параметров командной строки

Function ErrorCmdLine () 
    {
    Write-Host
    Write-Host "   Enter correctly parameters of a command line!"
    Write-Host "   Error file name $typefile"
    Write-Host 
    Write-Host "   EXAMPLE:"
    Write-Host "   Add_DNSRecord.ps1 file.csv log.txt"
    $logtext = "Error! Wrong name on the file $typefile"
    SendLogFile
    Exit
    }

# Функция отправки сообщений в лог файл
Function SendLogFile ()
    {
    $date = (Get-Date).ToString()
    $logstring = "$date - $logtext"
    try {
        Add-Content $logfile $logstring -ErrorAction stop
        }

    catch {
        Write-Host "  ERROR! Write error in a log file"
        Exit
        }

    finally {
        }

    }

Function SubnetIncorrect ()
    {
    Write-Host 
    Write-Host "   Error! The entered address of a subnet is incorrect"
    $logtext = "Error! The entered address of a subnet is incorrect"
    SendLogFile
    Exit
    }

Function CycleReadCSV ()
    {
    While ($ReadCount -lt $arrayDNSLength)
        {
  
        # Считывание и разделение строки на IP и Host
        $LineCSV = $arrayDNS[$ReadCount]
        $DNSRecord = $LineCSV.Split(";")
        $DNSIPRec = $DNSRecord[0] -replace "[\s]", ""
        $DNSHostRec = $DNSRecord[1] -replace "[\s]", ""
    
        # Разделение IP адреса на октеты
        $IPOctet = $DNSIPRec.Split(".")
        $IPOctetA = $IPOctet[0]
        $IPOctetB = $IPOctet[1]
        $IPOctetC = $IPOctet[2]
        $IPOctetD = $IPOctet[3]

        # Проверка количества октетов IP адреса, должно быть 4
        if ($IPOctet.Length -ne 4)
            {
            Write-Host
            Write-Host "   Error! In the column IP incorrect IP addresses"
            Write-Host "   EXAMPLE: 192.168.1.1"
            $logtext = "Error! In the column IP incorrect IP addresses"
            SendLogFile
            Exit
            }
        
        # Проверка октета A, считанного IP адреса
        if ($IPOctetA -ne $UserIPOctetA)
            {
            $logtext = "IP the address $DNSIPRec is incorrect, is not in the demanded subnet"
            SendLogFile
            
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }
            
        # Проверка октета B, считанного IP адреса
        If ($IPOctetB -ne $UserIPOctetB)
            {
            $logtext = "IP the address $DNSIPRec is incorrect, is not in the demanded subnet"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }            

        # Проверка октета C, считанного IP адреса
        If ($IPOctetC -ne $UserIPOctetC)
            {
            $logtext = "IP the address $DNSIPRec is incorrect, is not in the demanded subnet"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }

        # Проверка правильности имени хоста
        $DomainSuffix = $DNSHostRec.LastIndexOfAny(".")
        if ($DomainSuffix -ne -1)
            {
            $logtext = "Hostname $DNSHostRec is incorrect, the short name without DNS suffix is required."
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }
        
        # Проверка наличия уже аналогичной DNS записи
        $error.Clear()
        $DNSDomain = $GetDomainName.DNSRoot
        $DuplicateDNSRec = Get-DnsServerResourceRecord -ZoneName $DNSDomain -Name $DNSHostRec -ErrorAction SilentlyContinue
        $DNSRecFound = $Error.Count
        if ($DNSRecFound -ne 1)
            {
            $logtext = "Error! This record $DNSHostRec already is on the DNS server"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }

        # Добавление IN A и PTR записи на DNS сервер
        $Error.Clear()
        Add-DnsServerResourceRecordA -IPv4Address $DNSIPRec -Name $DNSHostRec -ZoneName $DNSDomain -CreatePtr -ErrorAction SilentlyContinue
        $DNSRecFound = $Error.Count
        if ($DNSRecFound -eq 1)
            {
            $logtext = "Error! Error at DNS record addition IP- $DNSIPRec Host- $DNSHostRec"
            SendLogFile
            $logtext = "PowerShell Error: 'n $error"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }
        # $DNSIPRec
        # $DNSHostRec
        $logtext = "Adding. The entry IN A and PTR is added $DNSHostRec - $DNSIPRec"
        SendLogFile
        
        # Автоинкремент счётчика цикла
        $ReadCount++


        }
    }
# Считывание параметров запуска скрипта
$csvfile = $args[0]
$logfile = $args[1]


# Удаление служебных символов из-за которых может возникнуть ошибка имён файлов
$csvfile = $csvfile -replace "[^\s\d\w.]", ""
$logfile = $logfile -replace "[^\s\d\w.]", ""

Write-Host "---------------------------------------------------------------------"
Write-Host " Script for add DNS record to domain                                 "
Write-Host "---------------------------------------------------------------------"
Write-Host " MOSCOW INDUSTRIAL BANK"
Write-Host "  Version 1.0                                                        "
Write-Host



# Проверка параметров командной строки
If(([string]::IsNullOrEmpty($csvfile)) -and ([string]::IsNullOrEmpty($logfile)))
    {
    ErrorCmdLine
    }

$logtext = ""
SendLogFile
$logtext = "Script for add DNS record to domain version 1.0 is Running"
SendLogFile

# Проверка введённых данных командной строки
$csvfilename = $csvfile.split(".")
if (($csvfilename.length -ne 2) -or ($csvfilename[1] -ne "csv"))
    {
    $typefile = "csv"
    ErrorCmdLine
    Exit
    }

$logfilename = $logfile.split(".")
if (($logfilename.length -ne 2) -or ($logfilename[1] -ne "txt"))
    {
    $typefile = "txt"
    ErrorCmdLine
    Exit
    }

# Логирование успешного принятия параметров коммандной строки
$logtext = "Parameters of a command line are accepted"
SendLogFile
$logtext = "Testing verification and availability of the $csvfile file"
SendLogFile
Write-Host "   Parameters of a command line are accepted."

# Проверка наличия файла CSV
try {
    $Testfile = Test-Path $csvfile
    if ($Testfile -ne 1) 
        {
    Write-Host "   Error! CSV file not found!"
    $logtext = "Error! CSV file is not found!"
    SendLogFile
    Exit
        }

    }

catch [System.Management.Automation.ItemNotFoundException]
    {
    Write-Host "   Error! CSV file not found!"
    $logtext = "Error! CSV file is not found!"
    SendLogFile
    Exit
    }

finally {
    }

# Логирование наличия файла CSV
$logtext = "CSV file is found"
SendLogFile

# Попытка чтения CSV файла и определения количества записей в нём
try {
    $arrayDNS = Get-Content $csvfile
    $arrayDNSLength = $arrayDNS.Length
    }

catch {
    Write-Host "   Error! Error of reading CSV file!"
    $logtext = "   Error! Error of reading CSV file!"
    SendLogFile
    Exit
    }

finally {
    }

# Логирование считывание CSV файла и определения количества записей в нём
Write-Host "   Opening CSV file: $csvfile"
$logtext = "Opening CSV file: $csvfile"
SendLogFile
$arrayDNSLengthLine = $arrayDNSLength - 1
Write-Host "   Found $arrayDNSLengthLine records"
$logtext = "Found $arrayDNSLengthLine records"
SendLogFile

# Проверка количества записей, не должно быть более 254!
If ($arrayDNSLength -gt 254)
    {
    Write-Host
    Write-Host "   ERROR! The number of records is exceeded!"
    Write-Host "   Limit 254 records."
    $logtext = "Error! The number of records is exceeded!"
    SendLogFile
    Exit
    }

# Считывание заголовка таблицы, разделение его на колонки (IP,Host) и удаление лишних пробелов
$titlecsv = $arrayDNS[0]
$titledns = $titlecsv.split(";")
$titleip = $titledns[0] -replace "[\s]" , ""
$titlehost = $titledns[1] -replace "[\s]", ""

# Проверка количества столбцов таблицы
if ($titledns.Length -ne 2)
    {
    Write-Host "   ERROR! The quantity of columns in the table has to be 2!"
    Write-Host "   Read the readme.txt file and lead the table to necessary conditions."
    $logtext = "Error! The quantity of columns in the table has to be 2!"
    SendLogFile
    Exit
    }


# Проверка правильности заголовка CSV таблицы. Колонка IP
if ($titleip -ne "IP")
    {
    Write-Host
    Write-Host "    ERROR! Error in the heading CSV of the table."
    Write-Host "    Error! The column IP isn't found!"
    $logtext = "Error! The column IP isn't found!"
    SendLogFile
    Exit
    }

# Проверка правильности заголовка CSV таблицы. Колонка Host
if ($titlehost -ne "Host")
    {
    Write-Host
    Write-Host "    ERROR! Error in the heading CSV of the table."
    Write-Host "    Error! The column Host isn't found!"
    $logtext = "Error! The column Host isn't found!"
    SendLogFile
    Exit
    }

# Запрос у пользователя доменного имени, чтобы система удостоверилась в тот ли домен требуется добавить записи
Write-Host
Write-Host "   Please, enter a domain name for addition of DNS records."
Write-Host "   EXAMPLE: D600"
Write-Host 
$userdomain = Read-Host "   Domain?"

# Определение имени домена на запущенном сервере
try {
$GetDomainName = Get-ADDomain
    }

catch {
    Write-Host
    Write-Host "   Error! Error when reading a name of the domain!"
    Write-Host "   The script is started on domain server Windows Server 2012 R2?"
    $logtext = "Error! Error when reading a name of the domain!"
    SendLogFile
    Exit
    }

finally {

    }

$DomainName = $GetDomainName.Name

# Проверка правильности ввода имени домена пользователем
$userdomain = $userdomain.ToLower()
if ($userdomain -ne $DomainName)
    {
    Write-Host
    Write-Host "   Error! You are not in the domain $userdomain"
    $logtext = "Error! Error of input of a name of the domain"
    SendLogFile
    Exit
    }

# Определение в какую подсеть добавлять DNS записи
Write-Host
Write-Host "   Please enter the subnet address."
Write-Host "   EXAMPLE: 10.20.86"
Write-Host
$UserSubnet = Read-Host "   Subnet?"

# Считывание и разделение на октеты адреса подсети, введённую пользователем.
$UserIPOctet = $UserSubnet.Split(".")
$UserIPOctetA = $UserIPOctet[0]
$UserIPOctetB = $UserIPOctet[1]
$UserIPOctetC = $UserIPOctet[2]

# Проверка правильности ввода пользователем адреса подсети по количеству октетов
If ($UserIPOctet.Length -ne 3)
    {
    SubnetIncorrect
    }

# Цикл считывания CSV таблицы
$ReadCount = 1

# Проверка правильности ввода пользователем октета A и октета B при 172й сети
switch ($UserIPOctetA)
    {
        10 {
            # Введен октет А - 10я сеть
            Write-Host
            Write-Host "   10 - The entered subnet $UserSubNet is accepted."
            CycleReadCSV
            Write-Host "   CSV file successfully processed"
            Write-Host
            }

        172 {
            # Введен октет A - 172я сеть
                        
            # Проверим октет B в 172й сети
            If (($UserIPOctetB -ge 16) -and ($UserIPOctetB -le 31))
                {
                Write-Host
                Write-Host "   172 - The entered subnet $UserSubNet is accepted."
                CycleReadCSV
                Write-Host "   CSV file successfully processed"
                Write-Host
                }
            Else
                {
                SubnetIncorrect
                }

            }

        192 {  
            # Введён октет A - 192я сеть
            
            # Проверим октет B в 192й сети
            If ($UserIPOctetB -eq 168)
                {
                Write-Host
                Write-Host "   192 - The entered subnet $UserSubNet is accepted."
                CycleReadCSV
                Write-Host "   CSV file successfully processed"
                Write-Host  
                }
            Else
                {
                SubnetIncorrect
                }
            }

        default {
            # Ниодно условие не совпало, тоесть октет не локальной сети 
            SubnetIncorrect
            }
    }



