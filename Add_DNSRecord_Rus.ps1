﻿<#
MOSCOW INDUSTRIAL BANK.
Data 29.07.2015
Script for added DNS record to DNS server
By Sharikov Pavel
#>

# Обьявление переменных не требуется, можно сразу писать значения в переменную. Система автоматические переменную превращает в массив если в неё попадает целый набор значений.

# Считывание строк из файла можно сделать с помощью простой инструкции.
# $data = Get-Content <имя файла в кавычках или переменная>
# $data[0] - считывание первой строки
# $data[1] - считывание второй строки и т.д.

# Функция отображения ошибки параметров командной строки

Function ErrorCmdLine () 
    {
    Write-Host
    Write-Host "   Введите правильные параметры командной строки!"
    Write-Host "   Ошибочный файл $typefile"
    Write-Host 
    Write-Host "   Пример:"
    Write-Host "   Add_DNSRecord_Rus.ps1 file.csv log.txt"
    $logtext = "Ошибка! Ошибочное имя файла типа $typefile"
    SendLogFile
    Exit
    }

# Функция отправки сообщений в лог файл
Function SendLogFile ()
    {
    $date = (Get-Date).ToString()
    $logstring = "$date - $logtext"
    try {
        Add-Content $logfile $logstring -ErrorAction stop
        }

    catch {
        Write-Host "  Ошибка! Ошибка записи в логфайл"
        Exit
        }

    finally {
        }

    }

Function SubnetIncorrect ()
    {
    Write-Host 
    Write-Host "   Ошибка! Введённый адрес подсети некорректен"
    $logtext = "Ошибка! Введённый адрес подсети некорректен"
    SendLogFile
    Exit
    }

Function CycleReadCSV ()
    {
    While ($ReadCount -lt $arrayDNSLength)
        {
  
        # Считывание и разделение строки на IP и Host
        $LineCSV = $arrayDNS[$ReadCount]
        $DNSRecord = $LineCSV.Split(";")
        $DNSIPRec = $DNSRecord[0] -replace "[\s]", ""
        $DNSHostRec = $DNSRecord[1] -replace "[\s]", ""
    
        # Разделение IP адреса на октеты
        $IPOctet = $DNSIPRec.Split(".")
        $IPOctetA = $IPOctet[0]
        $IPOctetB = $IPOctet[1]
        $IPOctetC = $IPOctet[2]
        $IPOctetD = $IPOctet[3]

        # Проверка количества октетов IP адреса, должно быть 4
        if ($IPOctet.Length -ne 4)
            {
            Write-Host
            Write-Host "   Ошибка! В столбце IP неправильный IP адрес"
            Write-Host "   Пример: 192.168.1.1"
            $logtext = "Ошибка! В столбце IP неправильный IP адрес"
            SendLogFile
            Exit
            }
        
        # Проверка октета A, считанного IP адреса
        if ($IPOctetA -ne $UserIPOctetA)
            {
            $logtext = "IP адрес $DNSIPRec неверен, он не входит в указанную подсеть"
            SendLogFile
            
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }
            
        # Проверка октета B, считанного IP адреса
        If ($IPOctetB -ne $UserIPOctetB)
            {
            $logtext = "IP адрес $DNSIPRec неверен, он не входит в указанную подсеть"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }            

        # Проверка октета C, считанного IP адреса
        If ($IPOctetC -ne $UserIPOctetC)
            {
            $logtext = "IP адрес $DNSIPRec неверен, он не входит в указанную подсеть"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }

        # Проверка правильности имени хоста
        $DomainSuffix = $DNSHostRec.LastIndexOfAny(".")
        if ($DomainSuffix -ne -1)
            {
            $logtext = "Имя хоста $DNSHostRec неверно, требуется короткое имя без DNS суффикса"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }
        
        # Проверка наличия уже аналогичной DNS записи
        $error.Clear()
        $DNSDomain = $GetDomainName.DNSRoot
        $DuplicateDNSRec = Get-DnsServerResourceRecord -ZoneName $DNSDomain -Name $DNSHostRec -ErrorAction SilentlyContinue
        $DNSRecFound = $Error.Count
        if ($DNSRecFound -ne 1)
            {
            $logtext = "Ошибка! Данная запись $DNSHostRec уже существует на DNS сервере"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }

        # Добавление IN A и PTR записи на DNS сервер
        $Error.Clear()
        Add-DnsServerResourceRecordA -IPv4Address $DNSIPRec -Name $DNSHostRec -ZoneName $DNSDomain -CreatePtr -ErrorAction SilentlyContinue
        $DNSRecFound = $Error.Count
        if ($DNSRecFound -eq 1)
            {
            $logtext = "Ошибка! Ошибка добавления на DNS сервер IP- $DNSIPRec Host- $DNSHostRec"
            SendLogFile
            $logtext = "PowerShell Error: 'n $error"
            SendLogFile
            # Автоинкремент счётчика цикла
            $ReadCount++
            Continue
            }
        # $DNSIPRec
        # $DNSHostRec
        $logtext = "Добавлено. DNS запись IN A и PTR добавлена: $DNSHostRec - $DNSIPRec"
        SendLogFile
        
        # Автоинкремент счётчика цикла
        $ReadCount++


        }
    }
# Считывание параметров запуска скрипта
$csvfile = $args[0]
$logfile = $args[1]


# Удаление служебных символов из-за которых может возникнуть ошибка имён файлов
$csvfile = $csvfile -replace "[^\s\d\w.]", ""
$logfile = $logfile -replace "[^\s\d\w.]", ""

Write-Host "---------------------------------------------------------------------"
Write-Host " Скрипт добавления DNS записей в домен                               "
Write-Host "---------------------------------------------------------------------"
Write-Host " Московский Индустриальный Банк"
Write-Host "  Версия 1.0                                                         "
Write-Host



# Проверка параметров командной строки
If(([string]::IsNullOrEmpty($csvfile)) -and ([string]::IsNullOrEmpty($logfile)))
    {
    ErrorCmdLine
    }

$logtext = ""
SendLogFile
$logtext = "Скрипт добавления DNS записей версии 1.0 запущен"
SendLogFile

# Проверка введённых данных командной строки
$csvfilename = $csvfile.split(".")
if (($csvfilename.length -ne 2) -or ($csvfilename[1] -ne "csv"))
    {
    $typefile = "csv"
    ErrorCmdLine
    Exit
    }

$logfilename = $logfile.split(".")
if (($logfilename.length -ne 2) -or ($logfilename[1] -ne "txt"))
    {
    $typefile = "txt"
    ErrorCmdLine
    Exit
    }

# Логирование успешного принятия параметров коммандной строки
$logtext = "Параметры командной строки успешно приняты"
SendLogFile
$logtext = "Проверка наличия и доступности файла $csvfile"
SendLogFile
Write-Host "   Параметры командной строки приняты"

# Проверка наличия файла CSV
try {
    $Testfile = Test-Path $csvfile
    if ($Testfile -ne 1) 
        {
    Write-Host "   Ошибка! CSV файл не найден!"
    $logtext = "Ошибка! CSV файл не найден!"
    SendLogFile
    Exit
        }

    }

catch [System.Management.Automation.ItemNotFoundException]
    {
    Write-Host "   Ошибка! CSV файл не найден!"
    $logtext = "Ошибка! CSV файл не найден!"
    SendLogFile
    Exit
    }

finally {
    }

# Логирование наличия файла CSV
$logtext = "CSV файл найден"
SendLogFile

# Попытка чтения CSV файла и определения количества записей в нём
try {
    $arrayDNS = Get-Content $csvfile
    $arrayDNSLength = $arrayDNS.Length
    }

catch {
    Write-Host "   Ошибка! Ошибка чтения CSV файла!"
    $logtext = "   Ошибка! Ошибка чтения CSV файла!"
    SendLogFile
    Exit
    }

finally {
    }

# Логирование считывание CSV файла и определения количества записей в нём
Write-Host "   Открытие CSV файла: $csvfile"
$logtext = "Открытие CSV файла: $csvfile"
SendLogFile
$arrayDNSLengthLine = $arrayDNSLength - 1
Write-Host "   Обнаружено $arrayDNSLengthLine записей"
$logtext = "Обнаружено $arrayDNSLengthLine записей"
SendLogFile

# Проверка количества записей, не должно быть более 254!
If ($arrayDNSLength -gt 254)
    {
    Write-Host
    Write-Host "   Ошибка! Превышено количество записей!"
    Write-Host "   Лимит 254 записи"
    $logtext = "Ошибка! Количество записей превышено!"
    SendLogFile
    Exit
    }

# Считывание заголовка таблицы, разделение его на колонки (IP,Host) и удаление лишних пробелов
$titlecsv = $arrayDNS[0]
$titledns = $titlecsv.split(";")
$titleip = $titledns[0] -replace "[\s]" , ""
$titlehost = $titledns[1] -replace "[\s]", ""

# Проверка количества столбцов таблицы
if ($titledns.Length -ne 2)
    {
    Write-Host "   Ошибка! Количество столбцов не равно 2!"
    Write-Host "   Читайте файл ReadMe.txt и приведите таблицу к требуемым параметрам"
    $logtext = "Ошибка! Количество столбцов не равно 2!"
    SendLogFile
    Exit
    }


# Проверка правильности заголовка CSV таблицы. Колонка IP
if ($titleip -ne "IP")
    {
    Write-Host
    Write-Host "    Ошибка! Ошибка заголовка CSV таблицы"
    Write-Host "    Ошибка! Столбец IP не найден!"
    $logtext = "Ошибка! Столбец IP не найден!"
    SendLogFile
    Exit
    }

# Проверка правильности заголовка CSV таблицы. Колонка Host
if ($titlehost -ne "Host")
    {
    Write-Host
    Write-Host "    Ошибка! Ошибка заголовка CSV таблицы"
    Write-Host "    Ошибка! Столбец Host не найден!"
    $logtext = "Ошибка! Столбец Host не найден!"
    SendLogFile
    Exit
    }

# Запрос у пользователя доменного имени, чтобы система удостоверилась в тот ли домен требуется добавить записи
Write-Host
Write-Host "   Пожалуйста, введите имя домена для добавления DNS записей"
Write-Host "   Пример: D600"
Write-Host 
$userdomain = Read-Host "   Domain?"

# Определение имени домена на запущенном сервере
try {
$GetDomainName = Get-ADDomain
    }

catch {
    Write-Host
    Write-Host "   Ошибка! Ошибка чтения имени домена!"
    Write-Host "   Скрипт запущен на доменном сервере Windows Server 2012 R2?"
    $logtext = "Ошибка! Ошибка чтения имени домена!"
    SendLogFile
    Exit
    }

finally {

    }

$DomainName = $GetDomainName.Name

# Проверка правильности ввода имени домена пользователем
$userdomain = $userdomain.ToLower()
if ($userdomain -ne $DomainName)
    {
    Write-Host
    Write-Host "   Ошибка! Вы находитесь не в домене $userdomain"
    $logtext = "Ошибка! Ошибка ввода имени домена"
    SendLogFile
    Exit
    }

# Определение в какую подсеть добавлять DNS записи
Write-Host
Write-Host "   Пожалуйста введите адрес подсети"
Write-Host "   Пример: 10.20.86"
Write-Host
$UserSubnet = Read-Host "   Subnet?"

# Считывание и разделение на октеты адреса подсети, введённую пользователем.
$UserIPOctet = $UserSubnet.Split(".")
$UserIPOctetA = $UserIPOctet[0]
$UserIPOctetB = $UserIPOctet[1]
$UserIPOctetC = $UserIPOctet[2]

# Проверка правильности ввода пользователем адреса подсети по количеству октетов
If ($UserIPOctet.Length -ne 3)
    {
    SubnetIncorrect
    }

# Цикл считывания CSV таблицы
$ReadCount = 1

# Проверка правильности ввода пользователем октета A и октета B при 172й сети
switch ($UserIPOctetA)
    {
        10 {
            # Введен октет А - 10я сеть
            Write-Host
            Write-Host "   Принята 10я подсеть $UserSubNet"
            CycleReadCSV
            Write-Host "   CSV файл успешно обработан"
            Write-Host		
            }

        172 {
            # Введен октет A - 172я сеть
                        
            # Проверим октет B в 172й сети
            If (($UserIPOctetB -ge 16) -and ($UserIPOctetB -le 31))
                {
                Write-Host
                Write-Host "   Принята 172я подсеть $UserSubNet"
                CycleReadCSV
                Write-Host "   CSV файл успешно обработан"
                Write-Host
                }
            Else
                {
                SubnetIncorrect
                }

            }

        192 {  
            # Введён октет A - 192я сеть
            
            # Проверим октет B в 192й сети
            If ($UserIPOctetB -eq 168)
                {
                Write-Host
                Write-Host "   Принята 192я подсеть $UserSubNet"
                CycleReadCSV
                Write-Host "   CSV файл успешно обработан"
                Write-Host  
                }
            Else
                {
                SubnetIncorrect
                }
            }

        default {
            # Ниодно условие не совпало, тоесть октет не локальной сети 
            SubnetIncorrect
            }
    }



